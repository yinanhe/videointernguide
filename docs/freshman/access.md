Hi, 欢迎来到通用视觉组，在这个页面，你将了解到刚入职所需要申请的所有权限。我们的计算资源都托管在商汤私有云上，使用本组计算资源，首先你需要**拥有一个商汤账户**。

## VPN权限

所有员工都默认开通了VPN权限，如果没有vpn权限，请询问你的mentor进行开通. 新申请VPN规则请前往[OA系统-VPN权限申请(商汤员工)](https://newoa.sensetime.com/spa/workflow/static4form/index.html?_rdm=1620474022576#/main/workflow/req?iscreate=1&workflowid=148523&_key=u9bnzd),你的已有策略可以查看[dashborad](https://it.sensetime.com/dashboard/ssl-vpn-acl/)

## 计算资源申请

请前往[商汤私有云-训练集群-分区权限申请](https://opencloud.sensetime.com/#/partition?682c9vozr=3a38ead17d5f4f96874e7ddee41afeb6)中申请本组的所有计算资源，在`分区名称`中分别输入`GVT`,`GVM`,`INTERN`进行查询和批量申请权限.

<img src="../_static/images/partition_access.png" alt="drawing"/>

使用计算资源时根据需求**按需申请，推荐根据分配来使用**，您也可以[点此查询](https://docs.google.com/spreadsheets/d/13q6-0RdIKASp3qKKdT3KbJQEI8nackE17fBVzqoeBKU/edit?usp=sharing)资源分配情况，
查询当前集群使用情况（使用/排队/总卡数）,请点击[GVT-GPU使用查询](http://quota.d5.sensetime.com/?department=GVT&cluster=&submit=%E6%8F%90%E4%BA%A4),[A100-40G使用查询](http://quota.d5.sensetime.com/?department=GVM&cluster=&submit=%E6%8F%90%E4%BA%A4)

目前本组拥有的计算资源：

| 集群 | 分区 | GPU | 数量 |
|  ---  |  ---   | ---   |  ---   |
| bj16 | GVT | TitanXp(混入1080) | 160 |
| bj17 | GVT | TitanXp | 152 |
| sh1988 | GVT | V100-32G | 200 |
| sh30 | GVT | 1080Ti | 53 |
| sh36 | GVT | 1080Ti | 148 |
| sh38 | GVT | 1080Ti | 272 |
| sh40 | GVT | 1080Ti | 140 |
| sh1024| GVM | A100-40G| 368 |
| projects| INTERN | A100-80G | 256|

## 组权限申请

- 为什么需要申请组权限？

  A: Linux操作系统对多用户的管理，是非常繁琐的，所以用组的概念来管理用户就变得简单，每个用户可以在一个独立的组，每个组也可以有零个用户或者多个用户。 通过申请组权限，可以**便于组内共享文件**， 访问组内同学的目录。

在**已经完全获得上述集群登陆权限后**，前往[商汤私有云-训练集群-Group申请](https://opencloud.sensetime.com/#/group?682c9vozr=dfccf094c46a49ee8866f89898b22c4a)所有集群的组`gvx`的权限

## 存储Quota申请

- 什么是存储quota

  我们在集群上使用Lustre 分布式文件系统，它的后端有一批巨大的存储服务器提供支持，但是每个人可以使用的用量还是**有限的**, 
  
  
在存储中我们又分有`lustre`存储和`cache`存储，其中`lustre`存储速度慢于`cache`, 但是`cache`存储默认底层无硬件冗余，请务必及时备份重要数据。 推荐在上海的集群中，将`代码,conda环境`等放在cache，将`checkpoint和数据`存放在lustre. 大存储数据需求推荐使用[ceph对象存储]()

默认已经为每个用户分配了150G的lustre和cache的存储quota，可以通过以下指令查看
```
swatch -q
```
<img src="../_static/images/quota.png" alt="drawing"/>


## 邮箱组

- 如果你是商汤员工：

    在outlook邮件群组，推荐加入的群组找到下述邮件群组，并申请加入，向gvx@sensetime.com发送邮件，所有邮件组的同事均可以收到邮件

    Email Group: `gvx`
    (**Tips**: You need ask 邵婧 to add your account to this email group)

- 如果你是上海人工智能实验实验室员工：

    在outlook邮件群组，推荐加入的群组找到下述邮件群组，并申请加入，向gvx-sh@pjlab.org.cn发送邮件，所有邮件组的同事均可以收到邮件

    Email Group: `gvx-sh`

## Confluence

组内的共享文档工具，一般添加邮件组后就会拥有组内confluence权限，如果还是没有找mentor求助即可
[INTERN的confluence空间](https://confluence.sensetime.com/display/INTERN/INTERN)

## Gitlab

请确认是否加入了我们的gitlab组，[agi-gvm](https://gitlab.bj.sensetime.com/agi-gvm)

如果发现没有权限，找mentor开
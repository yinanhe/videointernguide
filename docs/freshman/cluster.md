Hi，如果你来到这个页面，请确认您已经拥有了商汤集群权限

## 集群结构介绍

集群由管理节点和计算节点组成，使用过程中在管理节点通过Slurm向计算节点提交CPU或GPU任务，对于**管理节点和计算节点不开放ssh权限**，因此需要通过**跳板机**登录到管理节点，使用方法如下，注意关注登录后跳出的提示信息，有各方面的集群使用指南。

```
跳板机列表（任意选择，无对应关系）：
上海跳板机 jump-sh.sensetime.com
北京跳板机 jump-sh.sensetime.com
深圳跳板机 jump-sh.sensetime.com
VSCode跳板机 jump-vscode.sensetime.com
ssh username@JumpServer
eg: ssh [yinzhenfei@jump-sh.sensetime.com]
```

## Slurm集群介绍

一个集群包括1台管理节点和若干计算节点，所有计算节点都不能登录，管理节点需要经过跳转机登录。用户在管理节点上可以编辑文件、编译程序（比Pytorch，TensorFlow）等，注意**不要占用太多CPU/GPU资源**，尤其不要在管理节点上运行GPU任务，以免影响其他用户，否则集群运维哥哥会在公司大群里@你，避免社死。

集群使用 Slurm 管理任务的提交和资源分配，Slurm 会将所有计算节点划分成若干分区，每个分区需要单独申请权限，任务不能跨分区提交。**所以确保已经有GVT的分区权限**。集群一般都会包含一个 Test 分区，该分区的任务最多只能执行 1 小时或 2 小时，所有用户都可在 Test 分区提交任务。

集群使用 Lustre 分布式文件系统，所有节点都将 Lustre 挂载在自己的 /mnt/lustre 目录下，Lustre 主要包含一个 share 目录 /mnt/lustre/share_data 和用户目录 /mnt/lustre/yourname。

集群管理机与本地机器及其他管理机的 scp、rsync 等是关闭的，用户可以使用 ftp 传输数据。在本地可以使用 ftp://节点-ftp.sh.sensetime.com 访问管理节点，默认主目录是Lustre下自己的目录，并且包含一个share链接指向 /mnt/lustre/share 目录。 FTP的对应表可以查看[FTP映射表](##FTP端口映射)

## 集群常用软件路径
| 软件 | 使用 |
| --- | --- |
| gcc-4.9.4 | export PATH=/mnt/lustre/share/gcc/gcc-4.9.4/bin/:\$PATH ; export LD_LIBRARY_PATH=/mnt/lustre/share/gcc/gmp-4.3.2/lib:/mnt/lustre/share/gcc/mpfr-2.4.2/lib:/mnt/lustre/share/gcc/mpc-0.8.1/lib:\$LD_LIBRARY_PATH;|
| gcc-5.3.0 | export PATH=/mnt/lustre/share/gcc/gcc-5.3.0/bin/:\$PATH;export LD_LIBRARY_PATH=/mnt/lustre/share/gcc/gmp-4.3.2/lib:/mnt/lustre/share/gcc/mpfr-2.4.2/lib:/mnt/lustre/share/gcc/mpc-0.8.1/lib:\$LD_LIBRARY_PATH |
| gcc-7.4.0 | export PATH=/mnt/lustre/share/gcc-7.4.0/bin/:\$PATH;export LD_LIBRARY_PATH=/mnt/lustre/share/gcc/gmp-4.3.2/lib:/mnt/lustre/share/gcc/mpfr-2.4.2/lib:/mnt/lustre/share/gcc/mpc-0.8.1/lib:\$LD_LIBRARY_PATH |
| git-2.0.5 | export PATH=/mnt/cache/share/git-2.0.5/bin:\$PATH;export LD_LIBRARY_PATH=/mnt/cache/share/git-2.0.5/lib64:\$LD_LIBRARY_PATH |
| cmake-3.8|  export PATH=/mnt/lustre/share/cmake3.8/bin/:\$PATH |
| cmake-3.11|  export PATH=/mnt/lustre/share/cmake-3.11.0-Linux-x86_64/bin/:$PATH |
| cuda-9.2| export PATH=/mnt/cache/share/cuda-9.2/bin:\$PATH;export LD_LIBRARY_PATH=/mnt/cache/share/cuda-9.2/lib64:\$LD_LIBRARY_PATH|
| cuda-10.1| export PATH=/mnt/cache/share/cuda-10.1/bin:\$PATH;export LD_LIBRARY_PATH=/mnt/cache/share/cuda-10.1/lib64:\$LD_LIBRARY_PATH|
| cuda-11.0| export PATH=/mnt/cache/share/cuda-11.0/bin:\$PATH;export LD_LIBRARY_PATH=/mnt/cache/share/cuda-11.0/lib64:\$LD_LIBRARY_PATH|
| cuda-11.1| export PATH=/mnt/cache/share/cuda-11.1/bin:\$PATH;export LD_LIBRARY_PATH=/mnt/cache/share/cuda-11.1/lib64:\$LD_LIBRARY_PATH|
| cuda-11.2| export PATH=/mnt/cache/share/cuda-11.2/bin:\$PATH;export LD_LIBRARY_PATH=/mnt/cache/share/cuda-11.2/lib64:\$LD_LIBRARY_PATH|
| cuda-11.3| export PATH=/mnt/cache/share/cuda-11.3/bin:\$PATH;export LD_LIBRARY_PATH=/mnt/cache/share/cuda-11.3/lib64:\$LD_LIBRARY_PATH|
| ffmpeg-4.2.1 |  export PATH=/mnt/cache/share/ffmpeg-4.2.1/bin/:\$PATH |
| mpi-2.1 | export PATH=/mnt/lustre/share/openmpi-2.1.1/bin:\$PATHexport LD_LIBRARY_PATH=/mnt/lustre/share/openmpi-2.1.1/lib:\$LD_LIBRARY_PATH |
| miniconda3 | export PATH=/mnt/cache/share/spring/conda_envs/miniconda3/bin:\$PATH; export LD_LIBRARY_PATH=/mnt/cache/share/spring/conda_envs/miniconda3/lib:\$LD_LIBRARY_PATH |
| java | export JAVA_HOME=/mnt/lustre/share/jdk1.8.0_51; export PATH=\$JAVA_HOME/bin:\$PATH; export CLASSPATH=.:\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar |

## CONDA配置

创建文件`~/.condarc`，并将下列内容写入文件
```
channels:
 - defaults
show_channel_urls: true
default_channels:
 - https://pkg.sensetime.com/repository/conda-tsinghua-main
 - https://pkg.sensetime.com/repository/conda-tsinghua-r
 - https://pkg.sensetime.com/repository/conda-tsinghua-msys2
custom_channels:
 conda-forge: https://pkg.sensetime.com/repository/conda-tsinghua-cloud
 msys2: https://pkg.sensetime.com/repository/conda-tsinghua-cloud.
 bioconda: https://pkg.sensetime.com/repository/conda-tsinghua-cloud
 menpo: https://pkg.sensetime.com/repository/conda-tsinghua-cloud
 pytorch: https://pkg.sensetime.com/repository/conda-tsinghua-cloud
 pytorch-lts: https://pkg.sensetime.com/repository/conda-tsinghua-cloud
 simpleitk: https://pkg.sensetime.com/repository/conda-tsinghua-cloud
 open3d-admin: https://pkg.sensetime.com/repository/anaconda/
 fvcore: https://pkg.sensetime.com/repository/anaconda/
 iopath: https://pkg.sensetime.com/repository/anaconda/
 bottler: https://pkg.sensetime.com/repository/anaconda/
 pytorch3d: https://pkg.sensetime.com/repository/anaconda/
 aihabitat: https://pkg.sensetime.com/repository/anaconda/
```

## PIP 配置

创建pip配置文件
```
mkdir -p ~/.pip
touch ~/.pip/pip.conf
```
并将下列内容写入文件`~/.pip/pip.conf`
```
[global]
index-url = http://pypi.opencloud.sensetime.com/repository/pypi-proxy/simple/
[install]
trusted-host = pypi.opencloud.sensetime.com/
```

## git设置

可以使用集群中：/mnt/lustre/share/git 的脚本进行操作，可用于clone/pull等操作，禁用push等操作，**仅支持HTTPS协议**。

```
echo alias git='/mnt/lustre/share/git' >> ~/.bashrc
source ~/.bashrc
```


## 常用conda环境

### 共享环境（含spring)

包含有torch,linklink等常见的包, 不可使用conda install，pip安装需要--user, 但是是最简单的环境初始化

    ```
    source /mnt/cache/share/spring/$env_name
    如 source /mnt/cache/share/spring/s0.3.4
    ```

共享环境列表：
|环境名称|pytorch版本|备注|
|:----|:----|:----|
|s0.3.4|1.8.1+cuda112.cudnn8.1.0|spring 0.6.1+cu112.torch181.mvapich2.pmi2.nartgpu with auto model parallel, pytorch 1.8.0 with lms|
|s0.3.3/r0.3.3|1.5.0+cuda90.cudnn7.6.3.lms|linklink 0.3.1 with auto model parallel, pytorch 1.5.0 with lms|
|s0.3.2/r0.3.2|1.3.1+cuda90.cudnn7.6.3.lms|linklink 0.3.1 with auto model parallel, pytorch 1.3.1 with lms|
|r0.3.2|1.3.1+cuda90.cudnn7.6.3.lms|linklink 0.3.0 with model parallel, pytorch 1.3.1 with lms|
|r0.3.2 (preview)|1.3.0+cuda90.cudnn7.6.3|linklink 0.2.2, pytorch with cuda9.0, no LMS|
|r0.3.1|1.1.0+cuda90.cudnn7.6.3.lms|linklink更新到0.2.2，pytorch 1.1.0 with LMS (large model support), dali-0.14.0 支持POD custom数据读取|
|r0.3.0|1.1.0|linklink更新到0.2.1，支持 pytorch 1.1.0，dali-0.10.0|
|r0.2.2|0.4.1-cudnn-v7.4|linklink原地更新到0.1.7|
|r0.2.2|0.4.1-cudnn-v7.4|torch经过cudnn-v7.4.2编译|
| | |linklink更新到0.1.6|
|r0.2.1|0.4.1|linklink原地更新到0.1.5|
|r0.2.1|0.4.1|linklink原地更新到0.1.4|
|r0.2.1|0.4.1|torchE的syncBN已经完全迁移至linklink|
|r0.2.0|0.4.1|初始环境|


## 自定义一个conda环境
```
export PATH=/mnt/cache/share/spring/conda_envs/miniconda3/bin:$PATH
export LD_LIBRARY_PATH=/mnt/cache/share/spring/conda_envs/miniconda3/lib:$LD_LIBRARY_PATH
conda create -n py36 python=3.6
```

## 复制一个conda环境
```
export PATH=/mnt/cache/share/spring/conda_envs/miniconda3/bin:$PATH
export LD_LIBRARY_PATH=/mnt/cache/share/spring/conda_envs/miniconda3/lib:$LD_LIBRARY_PATH
conda create -n my0.3.4 --clone /mnt/cache/share/spring/conda_envs/miniconda3/envs/s0.3.4
```

## 如何在集群上提交任务

### Spring 提交任务

可以直接查看[spring空间](https://confluence.sensetime.com/pages/viewpage.action?pageId=242925857)

### PS-srun 提交任务

- 普通srun任务提交

    相比于直接在物理机上直接```python main.py```， 在slurm集群上提交任务需要额外使用srun来提交任务
    ```
    srun -p GVT --gres=gpu:8 -N1 echo helloworld
    ```
    运行这条指令，就会向计算节点申请8个gpu，并在控制台打印一个helloworld.

    ```
    srun -p GVT --gres=gpu:8 -N8 echo helloworld
    ```
    运行这条指令，就会向计算节点申请8个gpu(一个节点），并起了8个程序，每个程序在打印一个helloworld，您将看到8个helloworld

    ```
    srun -p GVT --gres=gpu:8 --ntasks-per-node=1 -N2 echo helloworld
    ```
    运行这条指令，就会向计算节点申请16个gpu(2个节点），并起了2个程序，每个程序单独出现在了一个节点上，且每个打印一个helloworld，您将看到2个helloworld

    ```
    srun -p GVT --gres=gpu:8 --quotatype=auto -N1 echo helloworld
    ```
    如果本组内一张卡也没有了，可以使用抢占的方式白嫖其他组的资源，使用方法为在后面加上--quotatype=auto, 如果其他组有卡空闲就会你的任务就会被调度

- 异步srun任务提交

    使用异步提交方式提交的任务支持被抢占（原本quota为spot，然后别人的任务排进来后kill了你的任务）后自动重新排队。

    使用方式为在原有的srun命令中添加`--async`选项即可。

    ```
    srun -p GVT --gres=gpu:8 --async --ntasks-per-node=1 -N2 echo helloworld
    ```
    任务会异步提交到计算节点，并生成默认使用当前工作路径，生成控制台输出，文件名为$pwd/phoenix-slurm-<jobid>.out

    提交任务时支持使用-o选项指定日志路径，-o选项后可以跟日志绝对路径或直接跟日志文件名



### sbatch 提交任务

sbatch用于提交作业到作业调度系统中。先编写作业脚本，然后使用sbatch命令提交。提交后脚本将在分配的计算节点中运行。

其优势是提交任务的方式为异步方式，退出tmux或终端连接不会对任务执行造成影响，同时支持在抢占后进行重排队。使用体验和srun --async差不多

需要编写一个sbatch的脚本，并将需要申请的资源放在脚本的最上方，举例如下：
```
#!/usr/bin/env sh
#SBATCH --partition=GVT
#SBATCH --gres=gpu:8
#SBATCH --ntasks-per-node=8
#SBATCH -n8
#SBATCH --job-name=train_efficient_b5
#SBATCH -o logs/log-train_efficient_b5-%j

GLOG_vmodule=MemcachedClient=-1 srun --mpi=pmi2 \
python -u main.py \
--config cfgs/effb5.yaml \
--load-path=pre_train_ckpt/imagenet.no-rand.pth.tar
```
然后直接在命令行中输入`sbatch $脚本名`, 系统将为你调度1个节点8gpu，并将log输出在logs/log-train_efficient_b5-%j

## 使用swatch来监控你的任务

通过`squeue -u yourname` 可以看到当前你正在运行的任务, 其中最后一列为你的任务`NODELIST(REASON)`所使用的节点$host，如`SH-IDC1-10-140-1-[145,152]`

### 查看top

执行这句会在目标节点上执行'top'以便于查看系统状态

```
# 短时间查看
swatch -n $host top
# 长时间查看（10分钟）
swatch -n $host top always
```

### 查看显卡利用率

执行这句会在目标节点上执行'nvidia-smi'以便于检测GPU status
```
# 短时间查看
swatch -n $host nv
# 长时间查看（10分钟）
swatch -n $host nv always
```

### 感觉节点有点卡，释放一下剩余内存

```
swatch -n $host memory_release
```
如果还是很卡，企业微信查找`集群助手`，请他帮忙分析


## 查看我们还有多少闲置显卡

### 网页端

查询当前集群使用情况（使用/排队/总卡数）,请点击[GVT-GPU使用查询](http://quota.d5.sensetime.com/?department=GVT&cluster=&submit=%E6%8F%90%E4%BA%A4),[A100-40G使用查询](http://quota.d5.sensetime.com/?department=GVM&cluster=&submit=%E6%8F%90%E4%BA%A4),[INTERN-80G-暂未上线]()

### 命令行

使用`svp list`
```
svp list

>VIRTUAL_PARTITION          DEPARTMENT                 RESERVED_TOTAL   RESERVED_USED   RESERVED_IDLE   SPOT_USED   RESERVED_BLOCKED
GVT                        <none>                     272              136             136             0           0
意思为总quota272张卡，使用136张，剩余136张，白嫖0张
```

## 查看我当前运行的任务

你可以使用

```
squeue -u yourname
```
<img src="../_static/images/squeue.png" alt="drawing"/>

- JOBID：任务号
- VIRTUAL_PAETITION：分区名字
- NAME： 任务名称
- QUOTA_TYPE：任务类型，reserved占用本分区资源，spot为白嫖其他分区模式，可以通过srun --quotatype指定，auto方式为有资源时分配reserved，没资源时spot
- USER： 用户名
- ST： R正在跑，PD 正在排队， CG正在退出
- TIME： 已经运行的时常，最大运行时间为14days
- NODES：使用的节点数量
- NODELIST：使用的节点ip列表

## 查看分区中的任务

查看提交在我们组分区的任务使用:

```
squeue -p GVT 
```

其中GVT为分区名字，看集群替换为GVM, INTERN

## 取消我的任务

取消单个任务
```
scancel $jobid
```
取消我的所有任务
```
scancel -u $yourname
```

## Ceph存储

强烈推荐使用ceph来放数据，询问你的同事获得ceph ak/sk 

如何使用请查看[sdk说明](https://confluence.sensetime.com/pages/viewpage.action?pageId=337847204)

## FTP端口映射

|集群名称|FTP地址|FTP域名信息|
|:----|:----|:----|
|cluster_sh1984|10.198.4.201|sh1984-ftp.sh.sensetime.com|
|cluster_15|10.10.15.46|bj15-ftp.bj.sensetime.com|
|cluster_16|10.10.16.90|bj16-ftp.bj.sensetime.com|
|cluster_17|10.10.17.72|bj17-ftp.bj.sensetime.com|
|cluster_30|10.10.30.69|bj30-ftp.bj.sensetime.com|
|cluster_shlg|10.198.32.11|shlg-ftp.sh.sensetime.com|
|cluster_sz10|10.112.2.11|sz10-ftp.sz.sensetime.com|
|cluster_hk75|10.1.75.55|hk75-ftp.hk.sensetime.com|
|cluster_sg1|10.51.0.30|sg1-ftp.sg.sensetime.com|
|cluster_sg2|10.51.3.34|sg2-ftp.sg.sensetime.com|
|cluster_abud|10.56.2.59|abud-ftp.abud.sensetime.com|
|cluster_sz20|172.20.20.22|sh20-ftp.sh.sensetime.com|
|cluster_sh30|10.5.30.31|sh30-ftp.sh.sensetime.com|
|cluster_sh34|10.5.34.23|sh34-ftp.sh.sensetime.com|
|cluster_sh36|10.5.36.170|sh36-ftp.sh.sensetime.com|
|cluster_sh38|10.5.39.62|sh38-ftp.sh.sensetime.com|
|cluster_sh40|10.5.40.181|sh40-ftp.sh.sensetime.com|
|cluster_sh1986|10.198.6.159|sh1986-ftp.sh.sensetime.com|
|cluster_sh1988|10.198.8.138|sh1988-ftp.sh.sensetime.com|
|cluster_sh1024|10.198.34.11|sh1024-ftp.sh.sensetime.com|
|cluster_sh1424|10.142.4.202|sh1424-ftp.sh.sensetime.com|


## HTTP服务端口访问

适用于：
- tensorborad
- jupyter
- httpserver等

**计算节点不适用，不要srun**

举个例子： 如果我想要登陆bj17集群，bj17的管理节点为10.10.17.32，而此时我要登陆的ftp从原来的 ~~lftp zhangsan@10.10.17.21~~ 变为了lftp zhangsan@10.10.17.72 而我开在10.10.17.32上的tensorboard/jupyter则需要监听12100-12399端口，并且从cluster-proxy.sh.sensetime.com:12100-12399访问此类服务。

端口映射表如下：

|集群名称|原访问地址|现有访问地址|
|:----|:----|:----|
|cluster_sh1984|10.198.4.31:10000-10299|cluster-proxy.sh.sensetime.com:10000-10299|
|cluster_sh1984|10.198.4.32:10300-10599|cluster-proxy.sh.sensetime.com:10300-10599|
|cluster_15|10.10.15.115:10600-10899|cluster-proxy.sh.sensetime.com:10600-10899|
|cluster_15|10.10.15.32:10900-11199|cluster-proxy.sh.sensetime.com:10900-11199|
|cluster_16|10.10.16.21:11200-11499|cluster-proxy.sh.sensetime.com:11200-11499|
|cluster_16|10.10.16.32:11500-11799|cluster-proxy.sh.sensetime.com:11500-11799|
|cluster_17|10.10.17.21:11800-12099|cluster-proxy.sh.sensetime.com:11800-12099|
|cluster_17|10.10.17.32:12100-12399|cluster-proxy.sh.sensetime.com:12100-12399|
|cluster_30|10.10.30.91:12400-12699|cluster-proxy.sh.sensetime.com:12400-12699|
|cluster_30|10.10.30.32:12700-12999|cluster-proxy.sh.sensetime.com:12700-12999|
|cluster_sz10|10.112.2.10:13000-13299|cluster-proxy.sh.sensetime.com:13000-13299|
|cluster_hk75|10.1.75.35:13300-13599|cluster-proxy.sh.sensetime.com:13300-13599|
|cluster_lg|10.198.32.69:13600-13899|cluster-proxy.sh.sensetime.com:13600-13899|
|cluster_sg1|10.51.0.103:13900-14199|cluster-proxy.sh.sensetime.com:13900-14199|
|cluster_sg2|10.51.3.30:14200-14499|cluster-proxy.sh.sensetime.com:14200-14499|
|cluster_abud|10.56.2.31:14500-14799|cluster-proxy.sh.sensetime.com:14500-14799|
|cluster_abud|10.56.2.32:14800-15099|cluster-proxy.sh.sensetime.com:14800-15099|
|cluster_sh34|10.5.34.21:15100-15399|cluster-proxy.sh.sensetime.com:15100-15399|
|cluster_sh34|10.5.34.32:15400-15699|cluster-proxy.sh.sensetime.com:15400-15699|
|cluster_sh36|10.5.36.31:15700-15999|cluster-proxy.sh.sensetime.com:15700-15999|
|cluster_sh36|10.5.36.32:16000-16299|cluster-proxy.sh.sensetime.com:16000-16299|
|cluster_sh38|10.5.38.31:16300-16599|cluster-proxy.sh.sensetime.com:16300-16599|
|cluster_sh38|10.5.38.32:16600-16899|cluster-proxy.sh.sensetime.com:16600-16899|
|cluster_sh1986|10.198.6.31:16900-17199|cluster-proxy.sh.sensetime.com:16900-17199|
|cluster_sh1986|10.198.6.32:17200-17499|cluster-proxy.sh.sensetime.com:17200-17499|
|cluster_sh1988|10.198.8.31:17500-17799|cluster-proxy.sh.sensetime.com:17500-17799|
|cluster_sh1988|10.198.8.32:17800-18099|cluster-proxy.sh.sensetime.com:17800-18099|
|cluster_sh20|172.20.20.21:18100-18399|cluster-proxy.sh.sensetime.com:18100-18399|
|cluster_sh20|172.20.20.32:18400-18699|cluster-proxy.sh.sensetime.com:18400-18699|
|cluster_sh30|10.5.30.231:18700-18999|cluster-proxy.sh.sensetime.com:18700-18999|
|cluster_sh30|10.5.30.32:19000-19299|cluster-proxy.sh.sensetime.com:19000-19299|
|cluster_sh40|10.5.40.31:19300-19599|cluster-proxy.sh.sensetime.com:19300-19599|
|cluster_sh40|10.5.40.32:19600-19899|cluster-proxy.sh.sensetime.com:19600-19899|
|cluster_sh1024|10.198.34.31:19900-20199|cluster-proxy.sh.sensetime.com:19900-20199|
|cluster_sh1024|10.198.34.32:20200-20499|cluster-proxy.sh.sensetime.com:20200-20499|
|cluster_sh1424|10.142.4.31:20500-20799|cluster-proxy.sh.sensetime.com:20500-20799|
|cluster_sh1424|10.142.4.32:20800-21099|cluster-proxy.sh.sensetime.com:20800-21099|



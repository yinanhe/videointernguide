## 近期高频提问

### Q1
- 使用spring.submit 无法提交任务了 怎么办？

- A1: 提示没有spring_scheduler这个partition的情况，请查看提交指令是否遗漏了 -p GVT 或者 -p GVM
- A2: 提示No such file or directory: '/etc/slurm/quota.conf'， 请更新spring.submit 版本, 截止2022年1月6日，更新的指令为 
```
pip install http://spring.sensetime.com/pypi/dev/packages/spring_aux-0.6.7.develop.2022_01_06t04_10.1924966f-py3-none-any.whl --user
echo "export PATH=~/.local/bin/:$PATH" >> ~/.bashrc
```

### Q2
- 我在上海集群起任务很慢怎么办？
- A：请确保代码和conda 环境都已经放置在cache; 

### Q3
- 我在Project S集群，代码环境都在cache了，为什么还是很慢呢？！
- A：如果你发现输出第一行的log很快，但是训练速度很慢，请确认是否使用了mc. 在project集群mc已经被废弃，如果需要使用petrel oss，辛苦重新安装一下petrel sdk:
```
pip install --user /mnt/cache/share_data/heyinan/srcs/petrel-oss-sdk-v2.2.1-2-g1505ef3-master.tar.gz
cp /mnt/lustre/share/pytcs/petrel_pymc.so ~/.local/lib/python3.6/site-packages/petrel_client/cache/mc/petrel_pymc.so
cp /mnt/cache/share_data/heyinan/srcs/petreloss.conf ~/
```
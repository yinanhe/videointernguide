## 科学上网

科学上网详见dx.sensetime.com

## 使用VSCode日常开发

VSCode中的remote-ssh套件支持带跳板机的远程开发环境，可以在远程环境上带来非常接近本地IDE的体验，大大提高开发效率。

<img src="../_static/images/vscode-remote.png" alt="drawing"/>

1. 安装VSCode远程开发套件(Remote Developmentms:vscode-remote.vscode-remote-extensionpack)

如果你是macos/linux用户，完成1后，建议直接跳到[## 一键登录集群](##一键登录集群)部分，一步配置！

2. 修改ssh的config文件

3. 在打开的.ssh/config文件中添加
```
Host JumpMachine
    HostName jump-vscode.sensetime.com
    # 你跳板机的用户名
    User $username

Host SH1988
    # 目标机的ip地址
    HostName 10.198.8.31
    # 你目标机的用户名
    User $username
    # 目标机登录端口 
    Port 22
    ProxyCommand ssh -q -W %h:%p JumpMachine
```
4. 将你本地的ssh公钥添加到目标机的authorized_keys中。一般来说，是在你本地的.ssh目录下的id_rsa.pub文件（如macos/linux用户就是`~/.ssh/id_rsa.pub`）的文件内容，追加到**集群管理节点**的`~/.ssh/authorized_keys`。注意，进行这个操作前请备份集群上的authorized_keys文件。 
    
    如果不行的话，在命令行中使用'ssh username@TargetMachine'测试跳板机是否设置正确，如果可以上去则VSCode一般也没问题

5. 点击vscode中的ssh tragets，输入验证码和密码就可以登陆。（如果老出现Timeout报错则去vscode setting里把time out时间从15秒改为150秒。）

## 使用Pycharm日常开发

我没用过，欢迎提交教程！

## Senselive

可视化工具

img: http://10.5.38.31:13002/

vid: http://10.5.38.31:13003/

你需要构建一个list,内容如下：

```
list:path+' ' +label，eg.
/mn/xx/xxx/jpg 1
/mn/xxx/xx.jpg 0
```

如果使用遇到问题，可以找`庆宏`修复

## 传文件

和集群传文件建议使用[FileZilla](https://filezilla-project.org/)或lftp

注意每天仅可以下载3G的内容，上传quota无限制，最大连接数为10台

## 使用LFTP在集群中互传文件

集群中默认已经安装了`lftp`，你可以通过以下命令进行连接，推荐在有文件的集群，连接到需要传输到的集群

### 连接ftp服务端
```
#命令格式：lftp 用户名@ftp地址:
#lftp 回车 --> open ftp地址 --> login 登录
 
$ lftp zhangsan@sh1988-ftp.sh.sensetime.com
Password:
$ lftp zhangsan@sh1988-ftp.sh.sensetime.com:~>
```

### 传输文件的指令
```
ls：查看服务器当前目录下的文件 
cd：切换服务器当前路径 
pwd：查看服务器当前路径
# 上传单个文件
$ lftp yourname@10.10.15.115:/> put file1
 
# 上传多个文件
$ lftp yourname@10.10.15.115:/> mput file1 file2
   
# 下载单个文件
$ lftp yourname@10.10.15.115:/> get file1
 
# 下载多个文件
$ lftp yourname@10.10.15.115:/> mget file1 file2
   
# 多线程下载文件
$ lftp yourname@10.10.15.115:/> pget -n 4 filename  
 
# 上传目录，即同步本地当前目录(或指定目录)到服务器
$ lftp yourname@10.10.15.115:/> mirror -R [local_dir]
   
# 下载目录到本地
$ lftp yourname@10.10.15.115:/> mirror dir
 
# 退出lftp
$ lftp yourname@10.10.15.115:/> exit
```


## 一键登录集群

使用Linux和MacOS的用户可以体验一下使用，spring.remote一键登录集群. 
```
pip install http://spring.sensetime.com/pypi/dev/packages/spring_aux-0.6.6.sunye.2021_08_31t15_05-py3-none-any.whl --user --upgrade
export PATH=~/.local/bin/:$PATH
spring.remote init
登陆集群时 spring.remote bash [cluste_rname(e.g. sh38)]
还有spring.remote task等有趣的功能^_^
```
如果使用遇到问题，可以找`逸楠`把你拉进答疑群


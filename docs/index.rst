.. open-gv documentation master file, created by
   sphinx-quickstart on Thu Jan  6 16:41:43 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

欢迎你来到GVX组!
===================================
这里是GVX Freshman Guidelines，请先阅读下面的所有内容，期望可以解答您100%的疑惑！如果您对本文档有疑问，欢迎企业微信随时提问！

.. toctree::
   :maxdepth: 2
   :caption: 需要申请的权限

   freshman/access.md

.. toctree::
   :maxdepth: 2
   :caption: 集群使用

   freshman/cluster.md
   
.. toctree::
   :maxdepth: 2
   :caption: 数据共享

   freshman/data.md

.. toctree::
   :maxdepth: 2
   :caption: 常用工具

   freshman/tools.md

.. toctree::
   :maxdepth: 2
   :caption: 常见问题

   freshman/faq.md



